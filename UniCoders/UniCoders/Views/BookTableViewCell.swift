//
//  BookTableViewCell.swift
//  UniCoders
//
//  Created by Rufat A on 5/5/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import UIKit

class BookTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewBookCover: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelAuthors: UILabel!
    @IBOutlet weak var labelNarrators: UILabel!
    @IBOutlet weak var viewBackground: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        UCUtils.makeCorners(layer: viewBackground.layer, radius: 0.0, brdWidth: 1.0, brdColor: UIColor.darkGray)
    }

    // overrride and block parent functionality to block selection
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        //super.setSelected(selected, animated: animated)
    }
    
    // overrride and block parent functionality to block highlighting

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        //super.setHighlighted(highlighted, animated: animated)
    }
    
    class func reuseCellIdentifier() -> String {
        return "BookCellIdentifier"
    }
}
