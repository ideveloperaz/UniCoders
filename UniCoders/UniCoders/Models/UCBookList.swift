//
//  UCBookList.swift
//  UniCoders
//
//  Created by Rufat A on 5/5/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import Foundation

// In complex cases we could use classes and have a weak ref. to source object here instead of hardcoded string

struct UCBookList {
    let title: String
    let urlToImage: String
    var bookList: [UCBook]
    var nextPage: String
}
