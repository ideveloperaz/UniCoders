//
//  BooksTableViewDelegate.swift
//  UniCoders
//
//  Created by Rufat A on 5/5/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import Foundation
import UIKit

// delegate that represents article table view behavioral pattern

class BooksTableViewDelegate: NSObject, UITableViewDelegate {
        
    deinit {
        print("deinit BooksTableViewDelegate")
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
}
