//
//  BooksDataSource.swift
//  UniCoders
//
//  Created by Rufat A on 5/5/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import AlamofireImage

// data source delegte for acticles table view

class BooksDataSource: NSObject, UITableViewDataSource {
    
    // structure that holds all received data
    
    var bookList: UCBookList?
    
    // distionary that will have all loaded images from web data and could be used by cell once they are downloaded
    
    var imageList: [String:UIImage?]
    
    // delegate to notify that web request complete and data parsed
    
    weak var delegate: UCUpateDelegate?
    
    // just a place holder image for missing images or when loading
    
    let placeholderImage: UIImage!
    
    override init() {
        bookList = nil
        imageList = [String:UIImage?]()
        placeholderImage = UIImage(named: "img_placeholder")!
    }

    deinit {
        print("deinit BooksDataSource")
    }
    
    // populate books data model from web request
    
    func populateFromJson(_ json: JSON) {
        
        // temporary array just to store data from current request
        
        var booksArray = [UCBook]();

        // parse JSON, here I assume that data must be by using "guard"
        // otherwies we have to use "if let ..." if that data could be empty
        // like I did for nextPage
        
        guard let theMeta = json["metadata"].dictionary else { return }
        guard let theListTitle = theMeta["title"]?.string else { return }
        guard let theListUrlToImage = (theMeta["cover"]?.dictionary)?["url"]?.string else { return  }
        
        // nextPage indicator
        
        var nextPage: String?
        if let theNextPage = json["nextPage"].string {
            nextPage = theNextPage
        }

        // get books array and parse their data
        
        guard let theConsumables = json["consumables"].array else { return }

        var i = 0
        for subJson in theConsumables {
            guard let theBookMeta = subJson["metadata"].dictionary else { return }

            guard let theTitle = theBookMeta["title"]?.string else { continue }
            guard let theAuthors = theBookMeta["authors"]?.array else { continue }
            guard let theNarrators = theBookMeta["narrators"]?.array else { continue }
            guard let theUrlToImage = (theBookMeta["cover"]?.dictionary)?["url"]?.string else { continue  }
            
            booksArray.append(UCBook(title: theTitle,
                                     authors: theAuthors.map({$0["name"].string!}),
                                     narrators: theNarrators.map({$0["name"].string!}),
                                     urlToImage: theUrlToImage))
            
            // loads image for current book entry and will reload table for that index 
            // once image will be loaded. If we already have that url then just reload row
            
            if let _ = self.imageList[theUrlToImage] {
                updateRow(position: i)
            } else {
                loadCellImage(theUrlToImage, withPosition: i)
            }
            i += 1
        }
        
        // if we could get books from request add to data model
        
        if booksArray.count > 0 {
            if bookList == nil {
                
                // for very first page goes here
                
                self.bookList = UCBookList(title: theListTitle,
                                           urlToImage: theListUrlToImage,
                                           bookList: booksArray,
                                           nextPage: nextPage ?? "")

                loadListImage(theListUrlToImage, andTitle: theListTitle)
            } else {
                
                // for nextPage goes here
                
                self.bookList?.bookList += booksArray
                self.bookList?.nextPage = nextPage ?? ""
            }
        }
    }
    
    // get selected book
    
    func getBook(_ atIndex: IndexPath) -> UCBook? {
        if let theBookList = bookList {
            return theBookList.bookList[atIndex.row]
        } else {
            return nil
        }
    }
    
    //  loads and set image and title for book list
    
    func loadListImage(_ url: String, andTitle title: String) {
        let placeholderImage = UIImage(named: "img_placeholder")!
        let downloadURL = URL(string: url)!
        
        if let theDelegate = delegate {
            theDelegate.attributesDidLoad(withURL: downloadURL, placeholderImage: placeholderImage, title: title)
        }
    }

    // loads image for cell
    
    func loadCellImage(_ url: String, withPosition position: Int) {
        let downloadURL = URL(string: url)!
        
        let imv = UIImageView()
        imv.af_setImage(
            withURL: downloadURL,
            placeholderImage: placeholderImage,
            filter: nil,
            imageTransition: .crossDissolve(0.5),
            completion: { [weak self] response in
                guard let strongSelf = self else { return }
                
                // if there is image available use it otherwise use placeholder image
                
                if let theImage = response.result.value {
                    strongSelf.imageList[url] = theImage
                } else {
                    strongSelf.imageList[url] = strongSelf.placeholderImage
                }
                
                strongSelf.updateRow(position: position)
        })
    }
    
    func updateRow(position: Int) {
        DispatchQueue.main.async(execute: { [weak self] () -> Void  in
            guard let strongSelf = self else { return }
            strongSelf.delegate?.cellImageDidLoad(IndexPath(row: position, section: 0))
        })
    }

    // MARK: UITableViewDataSource
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let theBookList = bookList {
            return theBookList.bookList.count
        } else {
            return 0
        }
    }
    
    // to optimize cell displaing images will be loaded outside of cellForRowAt method and table will be
    // reloaded only for loaded indexes for best performance
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BookTableViewCell.reuseCellIdentifier(), for: indexPath ) as! BookTableViewCell
        
        if let theBook = getBook(indexPath) {
            let authors = theBook.authors.joined(separator: ", ")
            let narrators = theBook.narrators.joined(separator: ", ")
            
            cell.labelTitle.text = theBook.title
            cell.labelAuthors.text = "By: \(authors)"
            cell.labelNarrators.text = "With: \(narrators)"
            
            // if data loaded at data source load image otherwise use placeholder
            
            if let theImage = self.imageList[theBook.urlToImage] {
                cell.imageViewBookCover.image = theImage
            } else {
                cell.imageViewBookCover.image = placeholderImage
            }
        }
        return cell
    }
        
}
