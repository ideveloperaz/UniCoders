//
//  UCBook.swift
//  UniCoders
//
//  Created by Rufat A on 5/5/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import Foundation

// structure that represents single book

struct UCBook {
    let title: String
    let authors: [String]
    let narrators: [String]
    let urlToImage: String
}
