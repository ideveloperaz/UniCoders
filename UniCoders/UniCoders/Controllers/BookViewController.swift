//
//  BookViewController.swift
//  UniCoders
//
//  Created by Rufat A on 5/5/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import UIKit

class BookViewController: UIViewController, UCUpateDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var labelListTitle: UILabel!
    @IBOutlet weak var imageViewCover: UIImageView!
    @IBOutlet weak var tableViewBooks: UCIntrinsicTableView!
    @IBOutlet weak var viewForHUD: UIView!

    // data provider for table view
    
    var booksDataSource: BooksDataSource!
    
    // delegate for table view
    
    var booksTableViewDelegate: BooksTableViewDelegate!
    
    // isLoading = true when data is started loading from web to prevent re-launch at scroll delegate
    
    var isLoading = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // load first page from web and populates data model
        
        loadData(nextPage: "")
    }
    
    deinit {
        booksDataSource = nil
        booksTableViewDelegate = nil
        print("deinit BookViewController")
    }
    
    // MARK: UCUpateDelegate
    
    // will be called when page data will be load at BooksDataSource
    
    func attributesDidLoad(withURL: URL, placeholderImage: UIImage, title: String) {
        imageViewCover.af_setImage(withURL: withURL, placeholderImage: placeholderImage)
        labelListTitle.text = title
    }
    
    // will be called when BooksDataSource will load image for given indexPath
    
    func cellImageDidLoad(_ indexPath: IndexPath) {
        tableViewBooks.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
    }

    // MARK: UIScrollViewDelegate
    
    // used to find when scroll offset will be at the bottom of page
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset 
        if distanceFromBottom < height {
            if !isLoading {
                isLoading = true
                lastCellWillDisplay()
            }
        }
    }
    
    // loads nextPage once scroll offset will be at the bottom by UIScrollViewDelegate
    
    func lastCellWillDisplay() {
        
        // load only if nextPage not empty
        
        if let theNextPage = booksDataSource.bookList?.nextPage,
            theNextPage.characters.count > 0 {
            loadData(nextPage: theNextPage);
        }
    }
    
    // MARK: UserDefined
    
    // creates daa source and delegate for table view
    
    func bindData() {
        
        // bind books table data source and its delegate
        
        booksDataSource = BooksDataSource()
        booksDataSource.delegate = self
        booksTableViewDelegate = BooksTableViewDelegate()
        tableViewBooks.dataSource = booksDataSource
        tableViewBooks.delegate = booksTableViewDelegate
    }

    // load data from web 
    
    func loadData(nextPage: String) {
        isLoading = true
        UCRequest.sharedInstance.getBooks(controller: self, viewForHUD: viewForHUD, nextPage: nextPage, isSilent: true, onSuccess: { [weak self] json  in
            guard let strongSelf = self else { return }
            
            // lets populate local data model from response
            
            strongSelf.booksDataSource.populateFromJson(json)
            strongSelf.tableViewBooks.reloadData()
            strongSelf.isLoading = false
        } )
    }
}
