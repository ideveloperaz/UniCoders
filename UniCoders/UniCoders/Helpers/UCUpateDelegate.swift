//
//  UCUpateDelegate.swift
//  UniCoders
//
//  Created by Rufat A on 5/6/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import UIKit

// MARK: Protocols declaration

protocol UCUpateDelegate: class {
    func attributesDidLoad(withURL: URL, placeholderImage: UIImage, title: String)
    func cellImageDidLoad(_ indexPath: IndexPath)
}
