//
//  UCRequest.swift
//  UniCoders
//
//  Created by Rufat A on 5/5/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

// This class used to handle all request to web
// This is Singleton class

class UCRequest: NSObject {
    
    // singleton instance of the class
    
    static let sharedInstance = UCRequest()
    
    // shared instance od Alamofire SessionManager
    
    var sessionManager: SessionManager?
    let debug = 0
    
    // init Alamofire session manager
    // private init - so there only one instance (singleton)
    
    private override init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    deinit {
        sessionManager = nil
        print("deinit UCRequest")
    }
    
    // base request function to handle all requests
    
    func request(
        useHud: Bool,
        viewForHUD: UIView,
        link : String,
        parameters: [String: String],
        controller: UIViewController,
        isSilent: Bool = true,
        onSuccess: @escaping (JSON)->Void
        ) {
        if useHud { UCUtils.showHUD(contentView: viewForHUD) }
        
        self.sessionManager?.request(link, method: .get, parameters: parameters)
            .responseJSON { [weak controller, weak viewForHUD, weak self] response in
                guard let strongVC = controller else { return }
                guard let strongSelf = self else { return }
                guard let strongViewForHUD = viewForHUD else { return }
                
                if useHud { UCUtils.hideHUD(contentView: strongViewForHUD); }
                
                strongSelf.parseResponse(response: response,
                                         strongController: strongVC,
                                         strongSelf: strongSelf,
                                         isSilent: isSilent,
                                         onSuccess: onSuccess,
                                         onFailure: { _ in })
        }
    }
    
    // parse response from web request and pass data to completion handler
    // use SwiftyJSON to parse response data safely
    
    func parseResponse(
        response: DataResponse<Any>,
        strongController: UIViewController,
        strongSelf: UCRequest,
        isSilent: Bool = false,
        onSuccess: @escaping (JSON)->Void,
        onFailure: @escaping (String)->Void
        ) {
        switch response.result {
        case .success(let value):
            let json = JSON(value)
            if strongSelf.debug == 1 { print(json) }
            
            if let theConsumables = json["consumables"].array,
                theConsumables.count > 0 {
                onSuccess(json)
            } else {
                if !isSilent { UCUtils.showAlert(title: "FAILURE", message: json["status"].stringValue, controller: strongController) }
                onFailure(json["status"].stringValue)
            }
        case .failure(let error):
            print(error)
            if !isSilent { UCUtils.showAlert(title: "FAILURE", message: error.localizedDescription, controller: strongController) }
            onFailure(error.localizedDescription)
        }
    }
    
    // function to get books from web
    
    func getBooks(
        controller: UIViewController,
        viewForHUD: UIView,
        nextPage: String = "",
        isSilent: Bool = true,
        onSuccess: @escaping (JSON)->Void
        ) {
        self.request(useHud: true, viewForHUD: viewForHUD, link: UCConfig.getLink(nextPage), parameters: [:], controller: controller, onSuccess: onSuccess)
    }
    
}
