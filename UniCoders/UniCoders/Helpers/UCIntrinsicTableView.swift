//
//  UCIntrinsicTableView.swift
//  UniCoders
//
//  Created by Rufat A on 5/5/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import Foundation
import UIKit

// this tableview subclass used to show its full height inside external scrollview

class UCIntrinsicTableView: UITableView {

    override var contentSize: CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override public var intrinsicContentSize: CGSize {
        get {
            self.layoutIfNeeded()
            return CGSize.init(width: UIViewNoIntrinsicMetric, height: contentSize.height+10)
        }
        
    }
    
}
