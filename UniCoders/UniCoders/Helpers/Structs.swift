//
//  Structs.swift
//  UniCoders
//
//  Created by Rufat A on 5/5/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import Foundation

// MARK: Configuration settings

struct UCConfig {
        
    // base url for books web site
    
    static let baseUrl = "http://api.storytelbridge.com/"
    
    // get links
    
    static func getLink(_ nextPage: String = "") -> String {        
        if nextPage.isEmpty {
            return "\(baseUrl)consumables/list/1"
        } else {
            return "\(baseUrl)consumables/list/1?page=\(nextPage)"
        }
    }
}

