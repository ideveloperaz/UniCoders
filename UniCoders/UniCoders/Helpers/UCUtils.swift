//
//  UCUtils.swift
//  UniCoders
//
//  Created by Rufat A on 5/5/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import UIKit
import MBProgressHUD

// different helper functions for code reuse purpose

class UCUtils: NSObject {
    
    // show alert helper
    
    class func showAlert(
        title: String,
        message: String,
        controller: UIViewController
        ) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
    // show alert helper with completion handler
    
    class func showAlert(
        title: String,
        message: String,
        controller: UIViewController,
        completion: @escaping ()->Void
        ) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
        { (action: UIAlertAction) -> Void in
            completion()
        } )
        controller.present(alert, animated: true, completion: nil)
    }
    
    // show loading indicator
    
    class func showHUD(contentView: UIView) {
        if contentView.viewWithTag(2121) == nil {
            UIActivityIndicatorView.appearance(whenContainedInInstancesOf: [MBProgressHUD.self]).color = UIColor.black
            
            let hud = MBProgressHUD.showAdded(to: contentView, animated: true)
            hud.mode = MBProgressHUDMode.indeterminate
            hud.bezelView.backgroundColor = UIColor.clear
            hud.label.textColor = UIColor.white
            hud.label.text = nil
            hud.tag = 2121
        }
        
    }
    
    // hide loading indicator
    
    class func hideHUD(contentView: UIView) {
        MBProgressHUD.hide(for: contentView, animated: true)
    }
    
    // make corners and borders
    
    class func makeCorners(layer: CALayer, radius: Float, brdWidth: Float, brdColor: UIColor) {
        layer.cornerRadius = CGFloat(radius)
        layer.borderWidth = CGFloat(brdWidth)
        layer.borderColor = brdColor.cgColor
    }

}
