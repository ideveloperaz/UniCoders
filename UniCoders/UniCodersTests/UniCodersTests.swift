//
//  UniCodersTests.swift
//  UniCodersTests
//
//  Created by Rufat A on 5/5/17.
//  Copyright © 2017 Rufat A. All rights reserved.
//

import XCTest
@testable import UniCoders

class UniCodersTests: XCTestCase, UCUpateDelegate {
    
    var expectationLoadData: XCTestExpectation!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    // test loading and parsing data from web request
    
    func testLoadData() {
        expectationLoadData = expectation(description: "Data loaded from web")
        
        let bookVC = createBookViewController()
        bookVC.viewDidLoad()
        bookVC.booksDataSource.delegate = self
        bookVC.loadData(nextPage: "")

        waitForExpectations(timeout: 5.0, handler: nil)

        let n = bookVC.booksDataSource.bookList?.bookList.count ?? -1
        
        XCTAssert(n > 0, "Data Not Loaded")
    }
    
    func testPerformanceExample() {
        self.measure {
        }
    }
    
    func createBookViewController() -> BookViewController
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "BookViewController") as! BookViewController
        vc.performSelector(onMainThread: #selector(vc.loadView), with: nil, waitUntilDone: true)
        return vc
    }
    
    // MARK: UCUpateDelegate
    
    // will be called when page data will be load at BooksDataSource
    
    func attributesDidLoad(withURL: URL, placeholderImage: UIImage, title: String) {
        expectationLoadData.fulfill()
    }
    
    // will be called when BooksDataSource will load image for given indexPath
    
    func cellImageDidLoad(_ indexPath: IndexPath) {
        //expectationLoadData!.fulfill()
    }
    
}
